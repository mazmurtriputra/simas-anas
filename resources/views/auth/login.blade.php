@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-light" style="text-align:center; background-color: #333;border-color: white;">{{ __('L O G I N - SIMASTUPEN') }}</div>

                <div class="card-body text-center" style="background-color: #333;">
                <!-- <img id="profile-img" class="profile-img-card" src="img/logotelkom.png" style="height: 5rem;margin-top: 0rem; text-align: center;margin-bottom: 3rem;background-color: white;border-radius: 1rem;"> -->
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right text-light">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="username" style="background-color: #333;" type="text" class="form-control @error('username') is-invalid @enderror text-light" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right text-light">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" style="background-color: #333;" type="password" class="form-control @error('password') is-invalid @enderror text-light" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label text-light" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-danger" style="background-color: #dc4666;">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-danger" href="{{ route('password.request') }}" style="background-color: #dc4666;">
                                        {{ __('Lupa Password ?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
